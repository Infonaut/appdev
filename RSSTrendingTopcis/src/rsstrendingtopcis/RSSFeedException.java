package rsstrendingtopcis;

/**
 *
 * @author MAGPIE
 *
 */
public class RSSFeedException extends Exception {

    public RSSFeedException(String message) {
        super(message);
    }

    public RSSFeedException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
