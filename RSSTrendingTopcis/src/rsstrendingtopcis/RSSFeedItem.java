package rsstrendingtopcis;

import java.util.ArrayList;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;

/**
 *
 * @author mars0318
 *
 */
public class RSSFeedItem {

    private String feedTitle;
    private String itemTitle;
    private String itemLink;
    private String itemDate;
    private String itemDescription;

    public RSSFeedItem(String feedTitle, Element items) {
        
                this.itemTitle = items.getChild("title").getTextNormalize();
                this.itemDescription = items.getChild("description").getText();
                this.itemLink = items.getChild("link").getText();
                this.itemDate = items.getChildText("pubDate");       
                
        }
    

    public RSSFeedItem() {
    }

    public RSSFeedItem(String feedTitle, String itemTitle, String itemLink, String itemDate, String itemDescription) {
        this.feedTitle = feedTitle;
        this.itemTitle = itemTitle;
        this.itemLink = itemLink;
        this.itemDate = itemDate;
        this.itemDescription = itemDescription;
    }

    public String getFeedTitle() {
        return feedTitle;
    }

    public void setFeedTitle(String feedTitle) {
        this.feedTitle = feedTitle;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemLink() {
        return itemLink;
    }

    public void setItemLink(String itemLink) {
        this.itemLink = itemLink;
    }

    public String getItemDate() {
        return itemDate;
    }

    public void setItemDate(String itemDate) {
        this.itemDate = itemDate;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    @Override
    public String toString() {

        String summaryString = new StringBuilder().append("<html><b>").append(feedTitle).append(" </b> ").append(itemTitle).append(" (").append(itemDate).append(")</html>").toString();

        return summaryString;

    }

}
