package rsstrendingtopcis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;

/**
 *
 * @author mars0318
 *
 */
public class RSSFeedItemExtractor {

    List feeds = new ArrayList<>();
    public List<RSSFeedItem> items = new ArrayList<>();

    RSSFeedItemExtractor(List<Document> feeds) {

    }

    public void getItems(List feeds) {
    }

    public static List<RSSFeedItem> extractItems(List<Document> feeds) throws RSSFeedException, JDOMException {

        List<RSSFeedItem> RSSitems = new ArrayList<>();
        List<Element> elementItems = new ArrayList<>();
        
        for (Document doc : feeds) {
            Element rss = doc.getRootElement();
            Element channel = null;
            channel = rss.getChild("channel");
            elementItems = channel.getChildren("item");
            
            for (Element item : elementItems) {
                String feedTitle = channel.getChild("title").getText();
                String itemTitle = item.getChild("title").getTextNormalize();
                String description = item.getChild("description").getText();
                String itemLink = item.getChild("link").getText();
                String itemDate = item.getChildText("pubDate");
                
                RSSitems.add(new RSSFeedItem(feedTitle, itemTitle, itemLink, itemDate, description));
                
            }
        }

        return RSSitems;

    }

    public RSSFeedItemExtractor() throws JDOMException, IOException, RSSFeedException {

    }

    public List getFeeds() {
        return feeds;
    }

    public List getItems() {

        return Collections.unmodifiableList(items);

    }

}
