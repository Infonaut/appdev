package rsstrendingtopcis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import proxypass.ProxyPass;

/**
 *
 * @author lewi0146
 */
public class RSSFeedRetriever {

    public static List<Document> getFeeds(String fileName) throws JDOMException, IOException, MalformedURLException, FileNotFoundException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, NoSuchPaddingException, IllegalBlockSizeException, IllegalBlockSizeException, BadPaddingException, RSSFeedException {
        return getFeeds(fileName, false);
    }

    public static List<Document> getFeeds(String fileName, boolean useProxy) throws JDOMException, IOException, MalformedURLException, FileNotFoundException, FileNotFoundException, InvalidKeyException, NoSuchAlgorithmException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, RSSFeedException {

        List<Document> feeds = new ArrayList<>();

        File f = new File(fileName);
        Scanner feedList = new Scanner(f);

        String feedType = feedList.nextLine();
        switch (feedType) {
            case "file":
                while (feedList.hasNext()) {
                    feeds.add(getFeedFromFile(f.getParentFile().getAbsolutePath() + File.separator + feedList.nextLine()));
                }
                break;
            case "url":
                while (feedList.hasNext()) {
                    feeds.add(getFeedFromURL(feedList.nextLine(), useProxy));
                }
                break;
            default:
                feeds = null;
                break;
        }

        return feeds;

    }

    public static Document getFeedFromFile(String fileName) throws JDOMException, IOException, RSSFeedException {

        SAXBuilder sax = new SAXBuilder();
        File file = new File(fileName);
        Document doc = sax.build(file);

        return doc;

    }

    public static Document getFeedFromURL(String urlName, boolean useProxy) throws MalformedURLException, JDOMException, IOException, FileNotFoundException, InvalidKeyException, NoSuchAlgorithmException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

        if (useProxy) {
            ProxyPass.setup("credentials");
        }

        SAXBuilder sax = new SAXBuilder();
        URL url = new URL(urlName);
        Document doc = sax.build(url);

        return doc;

    }
}
