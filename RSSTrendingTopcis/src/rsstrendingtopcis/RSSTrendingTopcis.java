package rsstrendingtopcis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author lewi0146
 */
public class RSSTrendingTopcis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, JDOMException, MalformedURLException, FileNotFoundException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, RSSFeedException {

        List<Document> feeds = RSSFeedRetriever.getFeeds("feeds" + File.separator + "rssfeed_filelist.txt");

        if (feeds != null) {
            System.out.println("numbers of feeds: " + feeds.size());
            for (Document feed : feeds) {
                System.out.println(feed);
            }
        } else {
            System.out.println("feed retriever returned null! probably couldn't find the file.");
        }

    }
}
