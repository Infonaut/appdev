/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rsstrendingtopcis;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author MAGPIE
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({rsstrendingtopcis.Test_RSSFeedItemExtractor.class, rsstrendingtopcis.Test_RSSFeedItem.class})
public class Phase_1_TestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
}
