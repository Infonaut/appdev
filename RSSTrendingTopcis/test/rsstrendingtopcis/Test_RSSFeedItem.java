package rsstrendingtopcis;

import assignmentunittestloader.UnitTestLoader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author mars0318
 */
public class Test_RSSFeedItem extends TestCase {

    RSSFeedItem instance;
    UnitTestLoader test;

    public Test_RSSFeedItem(String testName) {
        super(testName);

    }

    /**
     * Test of getFeedTitle method, of class RSSFeedItem.
     */
    public void testGetFeedTitle() {
        RSSFeedItem instance = new RSSFeedItem();
        instance.setFeedTitle("Tech News");
        String expResult = "Tech News";
        String result = instance.getFeedTitle();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFeedTitle method, of class RSSFeedItem.
     */
    public void testSetFeedTitle() {
        String expFeedTitle = "Netizen Herald";
        instance.setFeedTitle(expFeedTitle);
        String resultFeedTitle = instance.getFeedTitle();
        assertTrue(expFeedTitle.equals(resultFeedTitle));
    }

    /**
     * Test of getItemTitle method, of class RSSFeedItem.
     */
    public void testGetItemTitle() {
        String expItemTitle = "The AI Singularity Is Closer Than It Appears ...";
        instance.setItemTitle("The AI Singularity Is Closer Than It Appears ...");
        String resultItemTitle = instance.getItemTitle();
        assertTrue(expItemTitle.equals(resultItemTitle));
    }

    /**
     * Test of setItemTitle method, of class RSSFeedItem.
     */
    public void testSetItemTitle() {
        String itemTitle = "";
        instance.setItemTitle(itemTitle);
    }

    /**
     * Test of getItemLink method, of class RSSFeedItem.
     */
    public void testGetItemLink() {
        String expResult = "http://www.pcworld.com/article/2142080/windows-xp-expiration-couldnt-save-pc-market-in-q1.html#tk.rss_all";
        String result = instance.getItemLink();
        assertTrue(expResult.equals(result));
    }

    /**
     * Test of setItemLink method, of class RSSFeedItem.
     */
    public void testSetItemLink() {
        String itemLink = "";
        instance.setItemLink(itemLink);
    }

    /**
     * Test of getItemDate method, of class RSSFeedItem.
     */
    public void testGetItemDate() {
        String expResult = "Wed, 09 Apr 2014 16:49:00 -0700";
        String result = instance.getItemDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setItemDate method, of class RSSFeedItem.
     */
    public void testSetItemDate() {
        String itemDate = "";
        instance.setItemDate(itemDate);
    }

    /**
     * Test of getItemDescription method, of class RSSFeedItem.
     */
    public void testGetItemDescription() {
        final String EXP_RESULT = "<article>\n	<section class=\"page\">\n<p>\nThe looming threat of running an unsupported OS wasn’t enough to save PC shipments from continuing their slide in the first quarter.</p><p>\nWorldwide PC shipments fell by 4.4 percent in the first three months of the year to 73.4 million units, IDC <a href=\"http://www.idc.com/getdoc.jsp?containerId=prUS24794514\">said</a> on Wednesday.</p><p>\nThe expiration of Windows XP, support for which ended this week, did prompt some businesses to refresh their PCs. And slower demand for tablets, as the market becomes saturated, may have helped lift laptop sales slightly.</p><p>\nBut sales were down overall, and the trend toward more mobile devices is unlikely to stop, IDC said.</p><p class=\"jumpTag\"><a href=\"/article/2142080/windows-xp-expiration-couldnt-save-pc-market-in-q1.html#jump\">To read this article in full or to leave a comment, please click here</a></p></section></article>";
        String result = instance.getItemDescription();
        assertTrue(EXP_RESULT.equals(result));
    }

    /**
     * Test of setItemDescription method, of class RSSFeedItem.
     */
    public void testSetItemDescription() {
        String itemDescription = "";
        instance.setItemDescription(itemDescription);
    }

    /**
     * Test of toString method, of class RSSFeedItem.
     */
    public void testToString() {
        String expResult = "<html><b>null </b> Even the death of Windows XP failed to jolt the PC market in Q1 (Wed, 09 Apr 2014 16:49:00 -0700)</html>";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    @Override
    public void setUp() throws RSSFeedException {
        instance = null;

        try {
            Document doc = RSSFeedRetriever.getFeedFromFile("feeds/tech_feed_7_item.xml");

            Element rss = doc.getRootElement();

            // get the channel, element with the tag <channel>
            Element channel = rss.getChild("channel");

            // get the feed/channel title, tag => <title>
            String feedTitle = channel.getChild("title").getText();

            // get all the items contained in the channel
            List<Element> items = channel.getChildren("item");

            instance = new RSSFeedItem(feedTitle, items.get(0));

        } catch (JDOMException | IOException ex) {
            Logger.getLogger(Test_RSSFeedItem.class.getName()).log(Level.SEVERE, null, ex);
        }

        test = new UnitTestLoader();
        test.load("tech_feed_7_item.ut");
    }

}
