package rsstrendingtopcis;

import assignmentunittestloader.UnitTestLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lewi0146
 */
public class Test_RSSFeedItemExtractor {

    public Test_RSSFeedItemExtractor() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testExtract_size() throws RSSFeedException, JDOMException {
        String testFilePrefix = "tech_rssfeed_one_filelist";

        // load the expected test output
        UnitTestLoader test = new UnitTestLoader();
        test.load(testFilePrefix + ".ut");

        // load the test data
        List<Document> feeds = null;
        try {
            feeds = RSSFeedRetriever.getFeeds("feeds" + File.separator + testFilePrefix + ".txt");
            
            
        } catch (IOException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException | JDOMException e) {
            fail("Error should not occur!");
        }

        // test
        RSSFeedItemExtractor instance = new RSSFeedItemExtractor(feeds);
        RSSFeedItemExtractor.extractItems(feeds);
        int expResult = test.getNumberOfItems();
        int result = instance.getItems().size();
        assertEquals(expResult, result);

    }

    @Test(expected = RSSFeedException.class)
    public void testParse_xml_malformed() throws JDOMException, IOException, RSSFeedException {
        String testFilePrefix = "tech_feed_7_malformed";

        try {
            RSSFeedRetriever.getFeedFromFile("feeds" + File.separator + testFilePrefix + ".xml");
        } catch (JDOMException | IOException jDOMException) {
            throw new RSSFeedException("Test Malformed XML");
        }

    }

}
